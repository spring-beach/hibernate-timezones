criterion: transformation from serverZone (writing) int Server (reading) is as expected

# Reading Hibernate: null, Server: UTC // identical to Reading Hibernate: UTC, Server: UTC
# Reading Hibernate: UTC, Server: UTC (contract)
write contract, ok, id=32, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T11:00, hibernateZone=null, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write contract, ok, id=33, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T11:00, hibernateZone=UTC, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write bond,     xx, id=34, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T13:00, hibernateZone=CET, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write classic,  xx, id=35, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T14:00, hibernateZone=null, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
ok, id=36, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T12:00, hibernateZone=UTC, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
write classic,  xx, id=37, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T14:00, hibernateZone=CET, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02

# Reading Hibernate: CET, Server: UTC (bond)
write contract, xx, id=32, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T09:00, hibernateZone=null, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write contract, xx, id=33, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T09:00, hibernateZone=UTC, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write bond,     ok, id=34, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T11:00, hibernateZone=CET, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write classic,  ok, id=35, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T12:00, hibernateZone=null, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
xx, id=36, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T10:00, hibernateZone=UTC, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
write classic,  ok, id=37, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T12:00, hibernateZone=CET, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02

# Reading Hibernate: null, Server: CET // identical to Reading Hibernate: CET, Server: CET
# Reading Hibernate: CET, Server: CET (classic)

write contract, xx, id=32, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T11:00, hibernateZone=null, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write contract, xx, id=33, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T11:00, hibernateZone=UTC, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write bond,     ok, id=34, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T13:00, hibernateZone=CET, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
write classic,  ok, id=35, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T14:00, hibernateZone=null, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
xx, id=36, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T12:00, hibernateZone=UTC, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
write classic,  ok, id=37, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T14:00, hibernateZone=CET, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02

# Reading Hibernate: UTC, Server: CET (never used)

id=32, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T13:00, hibernateZone=null, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
id=33, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T13:00, hibernateZone=UTC, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
id=34, originalTime=2023-09-02T11:00, localDateTime=2023-09-02T15:00, hibernateZone=CET, serverZone=UTC, comment=PG: Paris time, localDate=2023-09-02
id=35, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T16:00, hibernateZone=null, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
id=36, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T14:00, hibernateZone=UTC, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
id=37, originalTime=2023-09-02T14:00, localDateTime=2023-09-02T16:00, hibernateZone=CET, serverZone=CET, comment=PG: Paris time, localDate=2023-09-02
