package org.tbuttler.timekeeper.timings;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TimingRepository extends CrudRepository<Times, Long> {
    List<Times> findByLocalDateTime(LocalDateTime localDateTime);

    List<Times> findByLocalDate(LocalDate localDate);


}
