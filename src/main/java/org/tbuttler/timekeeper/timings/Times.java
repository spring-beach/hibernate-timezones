package org.tbuttler.timekeeper.timings;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Times {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    private String originalTime;

    @Column
    private LocalDateTime localDateTime;

    @Column (nullable = true)
    private String hibernateZone;

    @Column
    private String serverZone;

    @Column
    private String comment;

    @Column
    private LocalDate localDate;

}
