package org.tbuttler.timekeeper.timings;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.util.TimeZone;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Profile("init")
@Slf4j
public class TimingExperiment implements CommandLineRunner {

    @Autowired
    private TimingRepository timingRepository;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Override
    public void run(String... args) throws Exception {
        String hibernateZone = (String)emf.getProperties().get(AvailableSettings.JDBC_TIME_ZONE);
        log.info("Hibernate is using time zone {}", hibernateZone);
        if(hibernateZone != null ) {
            log.info("TimeZone details are {}", TimeZone.getTimeZone(hibernateZone));
        }

        LocalDateTime timestamp = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);
        saveTiming(hibernateZone, timestamp);
//
        Thread.sleep(1000);
        readTimesToday(timestamp);
    }

    private void readTimesToday(LocalDateTime timestamp) {
        log.info("search for date {}", timestamp.toLocalDate());
        timingRepository.findByLocalDate(timestamp.toLocalDate()).forEach(t -> log.info("found timing {}", t));
    }

    private Times saveTiming(String hibernateZone, LocalDateTime localDateTime) {
        ZoneId zoneId = TimeZone.getDefault().toZoneId();
        Times times = Times.builder()
                .comment("PG: Paris time")
                .originalTime(localDateTime.toString())
                .localDateTime(localDateTime)
                .localDate(localDateTime.toLocalDate())
                .hibernateZone(hibernateZone)
                .serverZone(zoneId.toString())
                .build();
        return timingRepository.save(times);
    }
}
