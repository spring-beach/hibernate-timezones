package org.tbuttler.timekeeper;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class TimekeeperApplication {

    public static void main(String[] args) {

        setTimeZone();
        SpringApplication.run(TimekeeperApplication.class, args);
    }


//    @PostConstruct
    public static void setTimeZone() {

//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        TimeZone.setDefault(TimeZone.getTimeZone("CET"));
    }
}
